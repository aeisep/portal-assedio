import React, { useEffect, useState } from 'react';
import { Button, message, Select, Table } from 'antd';
import '../assets/css/Results.css';
import 'antd/dist/antd.css';
import axios from 'axios';
import { LoadingOutlined } from '@ant-design/icons';
import { BiFootball } from 'react-icons/bi';
import { FaPlaystation } from 'react-icons/fa';

let locale = {
  emptyText: (
    <span>
      <p>
        <span className="ball-span"><BiFootball size="84px" /></span> 
      </p>
      <span className="empty-txt">Escolhe um Grupo!</span>
    </span>
  )
};

const { Option } = Select;

async function fetchPlayerData(id) {
  return await axios.get(`https://liga-taca-isep-api.herokuapp.com/api/players/${id}`);
}

function Results() {
  const [headerInfo, setHeaderInfo] = useState([
    'Equipa 1',
    'Resultado',
    'Equipa 2',
    'Data/Hora do Jogo',
    'Grupo',
  ]);
  
  const columns = [
    {
      title: headerInfo[0],
      dataIndex: 'player1',
      key: 'player1',
      align: 'center'
    },
    {
      title: headerInfo[1],
      dataIndex: 'score',
      key: 'score',
      align: 'center',
      width: "15%",
      className: 'bold-text'
    },
    {
      title: headerInfo[2],
      dataIndex: 'player2',
      key: 'player2',
      align: 'center'
    },
    {
      title: headerInfo[3],
      dataIndex: 'dateTime',
      key: 'dateTime',
      align: 'center',
      width: '20%'
    },
  ];

  useEffect(() => {
    if (window.innerWidth <= 900) {
      setHeaderInfo([
        'Equipa 1',
        'R',
        'Equipa 2',
        'Data',
        'G',
      ]);
    } else {
      setHeaderInfo([
        'Equipa 1',
        'Resultado',
        'Equipa 2',
        'Data/Hora do Jogo',
        'Grupo',
      ]);
    }
  }, []);


  const spinIcon = <LoadingOutlined className="spin-icon" spin />

  const [dataSource, setDataSource] = useState([]);
  const [platform, setPlatform] = useState('PC');
  const [rounds, setRounds] = useState([]);
  const [round, setRound] = useState(["Grupos"]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const handleChange = function (round) {
    setRound(`${headerInfo[4]} ${round[1]}`);
    setLoading(true);
    axios
      .get(`https://liga-taca-isep-api.herokuapp.com/api/games/round/${round[0]}`)
      .then(async (response) => {
        const finishedGames = await Promise.all(
          response.data
            .filter(game => game.status === 1)
            .sort((game1, game2) => new Date(game1.dateHour) - new Date(game2.dateHour))
            .map(async (game, index) => {
              const player1 = (await fetchPlayerData(game.playerId1)).data.name;
              const player2 = (await fetchPlayerData(game.playerId2)).data.name;

              const [date, time] = new Date(game.dateHour).toLocaleString().split(',');
              const [hours, minutes] = time.split(':');
              return {
                key: index,
                player1: player1,
                score: `${game.score1} - ${game.score2}`,
                player2: player2,
                dateTime: `${date} ${hours}:${minutes}`
              }
            }));
        setDataSource(finishedGames);
      })
      .catch((error) => {
        setDataSource([]);
        console.error(`An error has ocurred: ${error}`);
      })
      .finally(() => {
        setLoading(false);
      })
  }

  useEffect(() => {
    setLoading(true);
    axios
      .get(`https://liga-taca-isep-api.herokuapp.com/api/rounds/active/${platform}`)
      .then((response) => {
        setRounds(response.data
          .sort((round1, round2) => round1.number - round2.number)
          .map((round) => { return { id: round.id, number: round.number } }));
      })
      .catch((error) => {
        setError(true);
        console.error(`An error has ocurred: ${error}`);
      })
      .finally(() => {
        setError(false);
        setLoading(false);
      })
  }, [platform]);

  useEffect(() => {
    if (error) {
      message.error({
        content: 'Não há informação disponível de momento. Por favor, tente mais tarde!'
      })
    }
    /* error message will not appear if this one
    is not here so we made it invisible
    :))))))))))))))))))))))))))))))))))) */
    message.error({
      content: '-',
      className: "hidden-message"
    });
  }, [error]);

  return (
    <div id="results">
      <div id="table-btn-group">
        <Select id="select-btn" value={round} onChange={handleChange}>
          {
            rounds.map((round) => (
              <Option key={round.id} className="regular-text" value={[round.id, round.number]} >
                {headerInfo[4]} {round.number}
              </Option>
            ))
          }
        </Select>
      </div>
      <Table
        className="regular-text results-table"
        dataSource={dataSource}
        columns={columns}
        onHeaderRow={() => { return { className: "bold-text header-font" } }}
        bordered={true}
        tableLayout="fixed"
        loading={{ indicator: spinIcon, size: "large", spinning: loading }}
        pagination={false}
        locale={locale}
      />
    </div>
  );
}

export default Results;