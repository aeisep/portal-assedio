import React from 'react';
import '../assets/css/Homepage.css';
import 'antd/dist/antd.css';
import cartaz from "../assets/img/cartaz.png";

function Homepage() {
    return (
            <img src={cartaz} className="poster" alt="Liga & Taça ISEP - Cartaz" />
    );
}

export default Homepage;