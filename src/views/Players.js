import React, { useState, useEffect } from 'react';
import 'antd/dist/antd.css';
import '../assets/css/Players.css';
import axios from "axios"
import { storage } from '../firebase/index.js'
import { Button, Card, message } from 'antd';
import { RiComputerLine } from 'react-icons/ri';
import { FaPlaystation } from 'react-icons/fa';
import { LoadingOutlined } from '@ant-design/icons';

const { Meta } = Card;

async function fetchImage(fileName) {
    return await storage.ref("").child(`${fileName}`).getDownloadURL();
}

function Players() {
    const [dataSource, setDataSource] = useState([]);
    const [platform, setPlatform] = useState('PC');
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    // useEffect(() => {
    //     document.getElementsByClassName("layout-content").item(0).classList.add("background-enhancer");
    // }, []);

    useEffect(() => {
        setLoading(true);
        axios
            .get(`https://liga-taca-isep-api.herokuapp.com/api/players/active/${platform}`)
            .then(async (response) => {
                setError(false);
                const players = await Promise.all(
                    response.data
                        .sort((player1, player2) => player1.name.split(' ')[0].localeCompare(player2.name.split(' ')[0]))
                        .map(async (player) => {
                            const name = player.name.split(' ');
                            const first = name[0];
                            const last = name.pop();

                            const image = await fetchImage(player.imageUrl);
                            
                            return {
                                name: player.name,
                                grupo: player.grupo,
                                info: player.degree,
                                image: image
                            }
                        }));
                setDataSource(players);
            })
            .catch((error) => {
                setError(true);
                console.error(`An error has ocurred: ${error}`);
            })
            .finally(() => {
                if (error) {
                    message.error({
                        content: 'Não há informação disponível de momento. Por favor, tente mais tarde!'
                    })
                }
                /* error message will not appear if this one
                is not here so we made it invisible
                :))))))))))))))))))))))))))))))))))) */
                message.error({
                    content: '-',
                    className: "hidden-message"
                });
                setLoading(false);
            })
    }, [platform, error]);

    

    return (
        <div id="players">
            <div className="index-gallery">
                {loading && <LoadingOutlined className="spin-icon spin-icon-enhancer" spin />}
                {!loading && dataSource.map((player) => (
                    <Card className="player-card">
                    <h3 className="player-card-text">
                        {player.name}
                    </h3>
                    <span>Grupo: {player.grupo}</span>
                    <Meta
                        description={player.info} />
                
                    <img src={player.image} alt={player.name} />
                </Card>
                    
                ))}
            </div>
        </div>
    );
}
export default Players;